The user needs Arduino 1.6.12 or later IDE

The user needs Android Studio 2.2.3
    - The user needs to download the Intel ReconJet SDK and ADB driver
    - Install the Android ADB driver on the system.
    
Ideas
    - HUD Information
        - Speed 
        - Battery Voltage
        - RPM
        - Oil Temp
        - Fuel level
        
    - Power control to:
        - Hand warmers
        - Lights
        - 
    
    - Auto Snow Plow control. 
        - When the ATV is placed in reverse or Neutral, the control unit should bring the plow to the full up position
            - A proximity switch should be used to locate the upper plow position (Home position)
        - When the ATV is shifted into gear the control unit should let it all the way out.
            - Fixed on a timer
        - There should be an in and out switch bypass if the rider decides to let it ou more
        - There should be a switch to disable the snow plow control
        
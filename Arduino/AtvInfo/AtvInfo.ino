/*
   Copyright (c) 2017 Chris Cholas.  All rights reserved.
   This library belongs to Chris Cholas. It is free to use 
   for non-commercial applications
*/
#include <CurieBLE.h>
#include "CurieIMU.h"
#include <CurieTime.h>
#include <EEPROM.h>

#define ONBOARD_LED_PIN 13
#define BATTERY_PIN A0
#define OIL_TEMP_PIN A1
#define H20_TEMP_PIN A2
#define GPS_RX_PIN D2
#define GPS_TX_PIN D3
#define DEFAULT_SCREEN_TIMEOUT 5000

BLEPeripheral blePeripheral;       // BLE Peripheral Device (the board you're programming)
BLEService batteryService("180F"); // BLE Battery Service
BLEService deviceInfoService("180A"); // BLE Device Info Service
BLEService atvInfoService("912cf30c-f6e2-4b5e-8f4f-98106448f5c6"); // BLE ATV Info Service
BLEService speedoService("912cf30c-f6e2-4b5e-8f4f-98106448f5c7"); // BLE ATV Info Service
BLEService timeService("afab612c-0c0d-4441-8f43-8c14628ddf56"); // create service
BLECharacteristic manufName("2A29", BLERead | BLENotify,12);
BLECharacteristic serialNumberInfo("2A25", BLERead | BLENotify,12);
BLECharacteristic firmwareRevInfo("2A26", BLERead | BLENotify,6);
BLECharacteristic hardwareRevInfo("2A27", BLERead | BLENotify,6);
BLECharacteristic softwareRevInfo("2A28", BLERead | BLENotify,6);
BLECharacteristic modelNumberInfo("2A24", BLERead | BLENotify,14);
BLEUnsignedCharCharacteristic oilTemp("43fed919-9552-4edb-845c-25d7334c51cc", BLERead | BLEWrite);
BLEUnsignedCharCharacteristic waterTemp("be848517-a312-43c9-bf30-d23ade927e50", BLERead | BLEWrite);
BLEUnsignedCharCharacteristic speedOmeter("be848517-a312-43c9-bf30-d23ade927e57", BLERead | BLEWrite);
BLECharacteristic timeUpdate("2A08", BLERead | BLEWrite, 7);
  
unsigned char manufacture[12] = {0x43,0x48,0x52,0x49,0x53,0x20,0x43,0x48,0x4F,0x4C,0x41,0x53};
unsigned char firmware[6] = {0x76,0x30,0x2E,0x30,0x2E,0x01};
unsigned char hardware[6] = {0x76,0x30,0x2E,0x30,0x2E,0x01};
unsigned char software[6] = {0x76,0x30,0x2E,0x30,0x2E,0x01};
unsigned char timeData[7] = {0x00,0x00,0x00,0x00,0x00,0x00,0x00};
unsigned char modelNumber[14] = {0x53,0x4D,0x41,0x52,0x54,0x43,0x4C,0x49,0x50,0x20,0x2E,0x32,0x32,0x33};
unsigned char serialNumber[12] = {0x38,0x34,0x36,0x38,0x33,0x45,0x30,0x30,0x30,0x43,0x33,0x45};  //Bluetooth MAC Address

// BLE Battery Level Characteristic"
BLEUnsignedCharCharacteristic batteryLevelChar("2A19",  // standard 16-bit characteristic UUID
                                               BLERead | BLENotify);     // remote clients will be able to
int oldBatteryLevel = 0;  // last battery level reading from analog input
long previousMillis = 0;  // last time the battery level was checked, in ms
long previousAmmoMillis = 0;  // last time the battery level was checked, in ms
boolean onBoardLedFlag = false;
uint32_t currentTime = 0;
uint32_t previousTime = 0;
uint32_t deltaTime = 0;
bool bLedState = false;
String inputString = "";         // a string to hold incoming data


int oilTempMeas = 0;
int waterTempMeas = 0;
int speedOmeterMeas = 0;

void setup() {
  inputString.reserve(200);
  inputString = "";

  Serial.begin(9600);    // initialize serial communication
  delay(3000);
  Serial.println(F("*************************Setup Parameters**************************************"));
  
  pinMode(ONBOARD_LED_PIN, OUTPUT);   // initialize the LED on pin 13 to indicate when a central is connected
  
  /* Set up the BLE Services */
  blePeripheral.setLocalName("ATVI");
  blePeripheral.setDeviceName("ATVI");
  blePeripheral.setAdvertisedServiceUuid(deviceInfoService.uuid());
  blePeripheral.addAttribute(deviceInfoService);
  blePeripheral.addAttribute(manufName);
  blePeripheral.addAttribute(serialNumberInfo);
  blePeripheral.addAttribute(firmwareRevInfo);
  blePeripheral.addAttribute(hardwareRevInfo);
  blePeripheral.addAttribute(softwareRevInfo);
  blePeripheral.addAttribute(firmwareRevInfo);
  blePeripheral.addAttribute(modelNumberInfo);

  //ATVInfo Information
  blePeripheral.setAdvertisedServiceUuid(atvInfoService.uuid());
  blePeripheral.addAttribute(atvInfoService);
  blePeripheral.addAttribute(oilTemp);
  blePeripheral.addAttribute(waterTemp);

  //Speedometer Service
  blePeripheral.setAdvertisedServiceUuid(speedoService.uuid());  // add the service UUID
  blePeripheral.addAttribute(speedoService);   // Add the BLE Battery service
  blePeripheral.addAttribute(speedOmeter); // add the battery level characteristic

  //Initialize Device Information for the BLE service
  manufName.setValue(manufacture,12);
  serialNumberInfo.setValue(serialNumber,12);
  firmwareRevInfo.setValue(firmware,6);
  hardwareRevInfo.setValue(hardware,6);
  softwareRevInfo.setValue(software,6);
  firmwareRevInfo.setValue(manufacture,12);
  modelNumberInfo.setValue(modelNumber,14);

  //Initialize ATV Info Parameters
  oilTemp.setValue(oilTempMeas);
  waterTemp.setValue(waterTempMeas);
  speedOmeter.setValue(speedOmeterMeas);

  //Time Service
  blePeripheral.setAdvertisedServiceUuid(timeService.uuid());  // add the service UUID
  blePeripheral.addAttribute(timeService);   // Add the BLE Battery service
  blePeripheral.addAttribute(timeUpdate); // add the battery level characteristic

  timeUpdate.setEventHandler(BLEWritten, timeCharacteristicWritten);
  //set an initial value for the characteristic
  timeUpdate.setValue(timeData,7);


  /* Now activate the BLE device.  It will start continuously transmitting BLE
     advertising packets and will be visible to remote BLE central devices
     until it receives a new connection */
  blePeripheral.begin();
  Serial.println("Bluetooth device active, waiting for connections...");
  Serial.println(F("IMU initialisation complete, waiting for events..."));
  Serial.println(F("*************************End Setup Parameters**********************************"));
  Serial.println(F(""));
  
}

void loop() {
    oilTempMeas = random(30,245);
    oilTemp.setValue(oilTempMeas);
    waterTempMeas = random(0,245);
    waterTemp.setValue(waterTempMeas);
    speedOmeterMeas = random(0,60);
    speedOmeter.setValue(speedOmeterMeas);
  
    currentTime = millis();
    deltaTime = currentTime - previousTime;
      if( deltaTime >= DEFAULT_SCREEN_TIMEOUT){
        getTime();
        previousTime = currentTime;
  
        if( bLedState ){
          digitalWrite(ONBOARD_LED_PIN, HIGH);
          bLedState = false;
        }
        else{
          digitalWrite(ONBOARD_LED_PIN, LOW);
          bLedState = true;
        }

        Serial.print(F("Speedometer: "));
        Serial.print(speedOmeterMeas);
        Serial.println(F(" mph"));

        Serial.print(F("Oil Temperature: "));
        Serial.print(oilTempMeas);
        Serial.println(F(" F"));

        Serial.print(F("Water Temperature: "));
        Serial.print(waterTempMeas);
        Serial.println(F(" F"));

      
      }
      
    // listen for BLE peripherals to connect:
    BLECentral central = blePeripheral.central();
  
    // if a central is connected to peripheral:
    if (central) {
      Serial.print("Connected to central: ");
      // print the central's MAC address:
      Serial.println(central.address());
      // turn on the LED to indicate the connection:
  
      digitalWrite(ONBOARD_LED_PIN, HIGH);
      
      // check the battery level every 200ms
      // as long as the central is still connected:
      while (central.connected()) {
        long currentMillis = millis();
        
        // if 200ms have passed, check the battery level:
        if (currentMillis - previousMillis >= 200) {
          previousMillis = currentMillis;
          updateBatteryLevel();

          oilTempMeas = random(30,245);
          oilTemp.setValue(oilTempMeas);
          waterTempMeas = random(0,245);
          waterTemp.setValue(waterTempMeas);
          speedOmeterMeas = random(0,60);
          speedOmeter.setValue(speedOmeterMeas);
          Serial.print(F("Speedometer: "));
          Serial.print(speedOmeterMeas);
          Serial.println(F(" mph"));
  
          Serial.print(F("Oil Temperature: "));
          Serial.print(oilTempMeas);
          Serial.println(F(" F"));
  
          Serial.print(F("Water Temperature: "));
          Serial.print(waterTempMeas);
          Serial.println(F(" F"));
        }
      }
      // when the central disconnects, turn off the LED:
      digitalWrite(ONBOARD_LED_PIN, LOW);
      Serial.print(F("Disconnected from central: "));
      Serial.println(central.address());
    }
}

/*******************************************************************************************************************************
 * 
 * Function: void updateBatteryLevel(void)
 * 
 * Description: This function is called to calculate the battery voltage
 * 
 * Parameters: None
 * 
 * Return Value: None
 *******************************************************************************************************************************/
void updateBatteryLevel() {
  /* Read the current voltage level on the A0 analog input pin.
     This is used here to simulate the charge level of a battery.
  */
  int battery = analogRead(A0);
  //Serial.println(battery);
  int batteryLevel = map(battery, 0, 777, 0, 100);

  if (batteryLevel != oldBatteryLevel) {      // if the battery level has changed
    Serial.print(F("Battery Level % is now: ")); // print it
    Serial.println(batteryLevel);
    batteryLevelChar.setValue(batteryLevel);  // and update the battery level characteristic
    oldBatteryLevel = batteryLevel;           // save the level for next comparison
  }
}

/*******************************************************************************************************************************
 * 
 * Function: void timeCharacteristicWritten(BLECentral& central, BLECharacteristic& characteristic)
 * 
 * Description: This function is called when the Central Device modifies the time characteristic. The new value is received and
 * the Curie RTC is updated. The function also prints the new time to the screen
 * 
 * Parameters: None
 * 
 * Return Value: None
 *******************************************************************************************************************************/
void timeCharacteristicWritten(BLECentral& central, BLECharacteristic& characteristic) {
  // central wrote new value to characteristic, update LED
  Serial.println(F("Time Characteristic event, written: "));
  if (timeUpdate.value()) {
    char timeValue[64];
    timeData[0] = characteristic[0];
    timeData[1] = characteristic[1];
    timeData[2] = characteristic[2];
    timeData[3] = characteristic[3];
    timeData[4] = characteristic[4];
    timeData[5] = characteristic[5];
    timeData[6] = characteristic[6];

    //0x07E10C070A1400
    int majorYear = 0;
    word majorYear2 = 0x0000;
    majorYear2 = timeData[0]<<8 & 0xFF00;
    majorYear2 = majorYear2 | timeData[1];    
    majorYear = int(majorYear2);

    setTime(timeData[4], timeData[5], timeData[6], timeData[2], timeData[3], majorYear);
    sprintf(timeValue, "%02d/%02d/%04d %02d:%02d:%02d", timeData[2], timeData[3], majorYear, timeData[4], timeData[5], timeData[6]);
    Serial.print(F("Time Update: "));
    Serial.println(timeValue);
  } 
}

/*******************************************************************************************************************************
 * 
 * Function: void getTine()
 * 
 * Description: This function retrieves the time from the CUrie RTC, updates the BLE service and prints to the console.
 * 
 * Parameters: None
 * 
 * Return Value: None
 *******************************************************************************************************************************/
void getTime(){
  int monthNow = month();
  int dayNow = day();
  int yearNow = year();
  int hourNow = hour();
  int minuteNow = minute();
  int secondsNow = second();
  char timeValue[64];
  int yearUpper = 0;
  int yearLower = 0;
  yearUpper = yearNow>>8 & 0xff;
  yearLower = yearNow & 0xff;

  timeData[0] = yearUpper;
  timeData[1] = yearLower;
  timeData[2] = monthNow;
  timeData[3] = dayNow;
  timeData[4] = hourNow;
  timeData[5] = minuteNow;
  timeData[6] = secondsNow;
    
  sprintf(timeValue, "%02d/%02d/%04d %02d:%02d:%02d", monthNow, dayNow, yearNow, hourNow, minuteNow, secondsNow);
  Serial.print(F("Time: "));
  Serial.println(timeValue);
  timeUpdate.setValue(timeData,7);
}

unsigned long eeprom_crc(void) {

  const unsigned long crc_table[16] = {
    0x00000000, 0x1db71064, 0x3b6e20c8, 0x26d930ac,
    0x76dc4190, 0x6b6b51f4, 0x4db26158, 0x5005713c,
    0xedb88320, 0xf00f9344, 0xd6d6a3e8, 0xcb61b38c,
    0x9b64c2b0, 0x86d3d2d4, 0xa00ae278, 0xbdbdf21c
  };

  unsigned long crc = ~0L;

  for (int index = 0 ; index < EEPROM.length()  ; ++index) {
    crc = crc_table[(crc ^ EEPROM[index]) & 0x0f] ^ (crc >> 4);
    crc = crc_table[(crc ^ (EEPROM[index] >> 4)) & 0x0f] ^ (crc >> 4);
    crc = ~crc;
  }
  return crc;
}


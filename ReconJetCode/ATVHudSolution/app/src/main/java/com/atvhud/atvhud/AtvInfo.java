package com.atvhud.atvhud;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.BluetoothGattDescriptor;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.LinearLayout;

import com.reconinstruments.os.HUDOS;
import com.reconinstruments.os.hardware.extsensor.ExternalSensorConnectionParams;
import com.reconinstruments.os.hardware.extsensor.HUDExternalSensorManager;

import java.util.HashMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;

@SuppressLint("NewApi")
public class AtvInfo extends Activity {
    private final static String TAG = "AtvInfoTest";

    private BluetoothGatt mBluetoothGatt;
    private BluetoothManager mBluetoothManager;
    private BluetoothAdapter mBluetoothAdapter;
    private String leDeviceName;
    private String leDeviceAddress;
    private String leStatusText;
    private ArrayList<BluetoothDevice> mDeviceList;

    private TextView titleHud;
    private TextView currentSpeed;
    private TextView oilTemp;
    private TextView waterTemp;
    //private String currentAmmoStr = "None";
    //private String previousAmmoStr = "None";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_atv_info);
    }

    @Override
    public void onResume()
    {
        super.onResume();
        Log.d(TAG, "onResume()");

        titleHud  = (TextView) findViewById(R.id.Title);
        currentSpeed  = (TextView) findViewById(R.id.text_speed_level);
        oilTemp  = (TextView) findViewById(R.id.text_oiltemp);
        waterTemp  = (TextView) findViewById(R.id.text_watertemp);

        titleHud.setText("ATV HUD");
        currentSpeed.setText("--");
        oilTemp.setText("--");
        waterTemp.setText("--");

        Intent intent = getIntent();
        mDeviceList = (ArrayList<BluetoothDevice>) getIntent().getSerializableExtra("deviceList");
        Log.e(TAG, "Got the device list");
        Log.e(TAG, "Is ArrayList Empty: "+mDeviceList.isEmpty());
        Log.e(TAG, "Is ArrayList Size: "+mDeviceList.size());


        if( mDeviceList.size()>0) {
            //Log.e(TAG, "There are stuff in the list");
            BluetoothDevice device2 = mDeviceList.get(0);
            leDeviceName = device2.getName();
            leDeviceAddress = device2.getAddress();
            Log.e(TAG, leDeviceName);
            Log.e(TAG, leDeviceAddress);


            //We have a list of devices that we found in the scan. Now try to attach to them and read the
            //Services and Characteristics

            Log.e(TAG, "Attemp to attach to the Bluetooth Manager");
            // Attempt BLE Connection
            mBluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
            if (mBluetoothManager != null)
            {
                mBluetoothAdapter = mBluetoothManager.getAdapter();
                if (mBluetoothAdapter == null || !mBluetoothAdapter.isEnabled())
                {
                    Log.e(TAG, "BT Adapter is null or not enabled!");
                }
            }
            else {
                Log.e(TAG, "Unable to retrieve BluetoothManager");
            }

            BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(leDeviceAddress);
            mBluetoothGatt = device.connectGatt(this, false, mGattCallback);
        }
    }

    @Override
    public void onPause()
    {
        Log.d(TAG, "onPause()");
        if (mBluetoothGatt != null)
        {
            mBluetoothGatt.disconnect();
            mBluetoothGatt.close();
            mBluetoothGatt = null;
        }
        super.onPause();
    }

    // Various callback methods defined by the BLE API.
    private final BluetoothGattCallback mGattCallback = new BluetoothGattCallback()
    {
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState)
        {
            if (newState == BluetoothProfile.STATE_CONNECTED)
            {
                Log.i(TAG, "Connected to GATT server.");
                Log.i(TAG, "Attempting to start service discovery:" + mBluetoothGatt.discoverServices());
                broadcastMessage("- Connected -");

                //Discover the services
                mBluetoothGatt.discoverServices();


            }
            else if (newState == BluetoothProfile.STATE_DISCONNECTED)
            {
                Log.i(TAG, "Disconnected from GATT server.");
                broadcastMessage("- Disconnected -");
            }
        }

        @Override
        // New services discovered
        public void onServicesDiscovered(BluetoothGatt gatt, int status)
        {
            String blah = "";

            if (status == BluetoothGatt.GATT_SUCCESS)
            {
                Log.i(TAG, "Services discovered.");
                broadcastMessage("- Services Discovered -");

                List<BluetoothGattService> services = gatt.getServices();
                for (BluetoothGattService service : services) {
                    Log.i(TAG, "Service: " + service.getUuid());

                    //Is this the ATV service?
                    if( new String( "Characteristic: " + service.getUuid() ).contains("912cf30c-f6e2-4b5e-8f4f-98106448f5c6") || new String( "Characteristic: " + service.getUuid() ).contains("912cf30c-f6e2-4b5e-8f4f-98106448f5c7") ) {
                        Log.i(TAG, "Found the ATV info or Speedo service");
                        List<BluetoothGattCharacteristic> characteristics = service.getCharacteristics();

                        Log.i(TAG, "Made it here");
                        for (BluetoothGattCharacteristic characteristic : characteristics) {
                            blah = "" + characteristic.getUuid();
                            Log.i(TAG, blah);

                            if (blah.contains("43fed919-9552-4edb-845c-25d7334c51cc") || blah.contains("be848517-a312-43c9-bf30-d23ade927e50") || blah.contains("be848517-a312-43c9-bf30-d23ade927e57")) {
                                gatt.readCharacteristic(characteristic);

                                for (BluetoothGattDescriptor descriptor : characteristic.getDescriptors()) {
                                    descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
                                    gatt.writeDescriptor(descriptor);
                                }
                            }
                        }
                    }
                }
            }
            else { Log.w(TAG, "onServicesDiscovered received: " + status); }
        }

        @Override
        // Result of a characteristic read operation
        public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status)
        {
            try {
                if (status == BluetoothGatt.GATT_SUCCESS) {
                    Log.i(TAG, "Data is available.");
                    broadcastMessage("- Data Available -");
                    String blah = "" + characteristic.getUuid();
                    Log.i(TAG, blah);

                    //Current Oil Temp
                    if (blah.contains("43fed919-9552-4edb-845c-25d7334c51cc")) {
                        byte[] dataReturned = characteristic.getValue();
                        int dataRead = dataReturned[0] & 0x7F;
                        //Log.i(TAG, "Characteristic Length: " + dataReturned.length);
                        Log.i(TAG, "Current Oil Temp Value: " + dataRead);

                        uiOilMessage("" + dataRead);
                        gatt.readCharacteristic(characteristic);
                    }

                    //Current Water Temp
                    else if(blah.contains("be848517-a312-43c9-bf30-d23ade927e50")){
                        byte[] dataReturned = characteristic.getValue();
                        int dataRead = dataReturned[0] & 0x7F;
                        Log.i(TAG, "Current Water Temp Value: " + dataRead);

                        uiWaterMessage("" + dataRead);
                        gatt.readCharacteristic(characteristic);
                    }

                    //Current Speed
                    else if(blah.contains("be848517-a312-43c9-bf30-d23ade927e57")){
                        byte[] dataReturned = characteristic.getValue();
                        int dataRead = dataReturned[0] & 0x7F;
                        Log.i(TAG, "Current Speed Value: " + dataRead);

                        uiMessage("" + dataRead);
                        gatt.readCharacteristic(characteristic);
                    }
                }
            }

            catch (Exception e) {
                Log.e(TAG, e.getMessage());
            }
        }

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt, final BluetoothGattCharacteristic characteristic) {
            //read the characteristic data
            Log.i(TAG, "Characteristic Changed.");
            Log.i(TAG, "" + characteristic.getUuid());
        }
    };


    private void uiMessage(final String msg)
    {
        Message strMsg = new Message();
        strMsg.obj = msg;
        uiUpdateHandler.sendMessage(strMsg);
    }

    private Handler uiUpdateHandler = new Handler() {
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            currentSpeed.setText((String) msg.obj);
        }
    };

    private void uiWaterMessage(final String msg)
    {
        Message strMsg = new Message();
        strMsg.obj = msg;
        uiUpdateWaterHandler.sendMessage(strMsg);
    }

    private Handler uiUpdateWaterHandler = new Handler() {
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            waterTemp.setText((String) msg.obj);
        }
    };

    private void uiOilMessage(final String msg)
    {
        Message strMsg = new Message();
        strMsg.obj = msg;
        uiUpdateOilTemp.sendMessage(strMsg);
    }

    private Handler uiUpdateOilTemp = new Handler() {
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            oilTemp.setText((String) msg.obj);
        }
    };


    private void broadcastMessage(final String msg)
    {
        Message strMsg = new Message();
        strMsg.obj = msg;
        messageHandler.sendMessage(strMsg);
    }

    private Handler messageHandler = new Handler()
    {
        public void handleMessage(Message msg)
        {
            super.handleMessage(msg);
            leStatusText = (String) msg.obj;
        }
    };

}